﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication45.Core.Repositories
{
    public interface ISqlTransaction : IDisposable
    {
        void Commit();
        void RollBack(); 
    }
}
