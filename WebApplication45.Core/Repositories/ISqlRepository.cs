﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebApplication45.Core.Repositories
{
    public interface ISqlRepository<T> where T : class 
    {
        T Get();
        void Update(T entity);
        void Remove(T entity);
        T Add(T entity);
        IEnumerable<T> GetWhere(Func<T, bool> func); 
    }
}
