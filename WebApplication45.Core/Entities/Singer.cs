namespace WebApplication45
{
    public class Singer
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public int Performance { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; } 
    }
}