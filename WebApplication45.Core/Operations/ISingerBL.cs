﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace WebApplication45.Core.Operations
{
    public interface ISingerBL
    {
        IEnumerable GetSingers();
        Singer AddSingers(Singer singer);
        bool RemoveSingers(int id);
        bool UpdateSingers(Singer singer, int id); 
    }
}
