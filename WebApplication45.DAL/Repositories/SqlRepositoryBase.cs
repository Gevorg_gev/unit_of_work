﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WebApplication45.Core.Repositories;

namespace WebApplication45.DAL.Repositories
{
    public class SqlRepositoryBase<T> : ISqlRepository<T> where T : class
    {
        private readonly SingerDbContext singerDbContext;
        public SqlRepositoryBase(SingerDbContext singerDbContext)
        {
            this.singerDbContext = singerDbContext; 
        }
        public T Add(T entity)
        {
            singerDbContext.Set<T>().Add(entity);
            return entity; 
        }

        public T Get()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<T> GetWhere(Func<T, bool> func)
        {
            return singerDbContext.Set<T>().Where(func); 
        }

        public void Remove(T entity)
        {
            singerDbContext.Set<T>().Remove(entity);
        }

        public void Update(T entity)
        {
            singerDbContext.Set<T>().Update(entity); 
        }

        public int SaveChanges()
        {
            return singerDbContext.SaveChanges(); 
        }
    }
}
