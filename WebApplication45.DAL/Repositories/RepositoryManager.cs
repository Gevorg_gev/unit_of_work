﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using WebApplication45.Core.Repositories;

namespace WebApplication45.DAL.Repositories
{
    public class RepositoryManager : IRepositoryManager
    {
        private readonly SingerDbContext _singerDbContext;

        public RepositoryManager(SingerDbContext singerDbContext)
        {
            _singerDbContext = singerDbContext; 
        }

        private ISingerRepository _singers;

        public ISingerRepository Singers => _singers ?? (_singers = new SingerRepository(_singerDbContext));

        public int SaveChanges()
        {
            return _singerDbContext.SaveChanges(); 
        }

        public Task<int> SaveChangesAsync()
        {
            return _singerDbContext.SaveChangesAsync();
        }

        public ISqlTransaction BeginTransaction(IsolationLevel isolation = IsolationLevel.ReadCommitted) 
        {
            return SqlTransaction.Begin(_singerDbContext, isolation); 
        }
    }
}
