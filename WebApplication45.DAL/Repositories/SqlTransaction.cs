﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using WebApplication45.Core.Repositories;


namespace WebApplication45.DAL.Repositories
{
    public class SqlTransaction : ISqlTransaction
    {
        private readonly IDbContextTransaction _transaction;

        private SqlTransaction(SingerDbContext context, System.Data.IsolationLevel level)
        {
            _transaction = context.Database.BeginTransaction(level); 
        }

        public static ISqlTransaction Begin(SingerDbContext context, System.Data.IsolationLevel level)
        {
            return new SqlTransaction(context, level); 
        }
        public void Commit()
        {
            _transaction.Commit();
        }

        public void Dispose(bool v)
        {
            Dispose(true);
            GC.SuppressFinalize(this); 
        }

        protected virtual void Disposed(bool disposing) 
        {
            if (disposing)
            {
                _transaction.Dispose();
            } 
        }

        public void RollBack()
        {
            _transaction.Rollback(); 
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
