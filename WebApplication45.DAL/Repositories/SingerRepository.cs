﻿using System;
using System.Collections.Generic;
using System.Text;
using WebApplication45.Core.Repositories;

namespace WebApplication45.DAL.Repositories
{
    public class SingerRepository : SqlRepositoryBase<Singer> , ISingerRepository
    {
        public SingerRepository(SingerDbContext singerDbContext) : base(singerDbContext) 
        {

        }
    }
}
