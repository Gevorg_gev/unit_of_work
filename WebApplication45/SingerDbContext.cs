using Microsoft.EntityFrameworkCore;

namespace WebApplication45
{
    public class SingerDbContext : DbContext
    {
        public SingerDbContext(DbContextOptions options) : base(options)
        {

        }
        public DbSet<Singer> Singers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Singer>(x =>
            {
                x.ToTable("Singers");
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        } 
    }
}