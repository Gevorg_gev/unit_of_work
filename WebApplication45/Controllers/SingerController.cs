using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication45
{
    [ApiController]
    [Route("[controller]")] 
    public class SingerController : ControllerBase
    { 
        private readonly SingerDbContext singerDbContext;
        public SingerController(SingerDbContext singerDbContext)
        {
            this.singerDbContext = singerDbContext; 
        }

        private static readonly string[] Singer_Name = new[]
        {
            "Ariana" , "Alicia" , "John" , "Kelly" , "Adam" , "Gwen" 
        };

        private static readonly string[] Singer_Surname = new[]
        {
            "Grande" , "Keys" , "Legend" , "Clarkson" , "Smith" , "Stephanie" 
        };

        private static readonly string[] Singer_Country = new[]
        {
            "USA" 
        };

        [HttpGet]
        public IEnumerable GetSingers()
        {
            var rng = new Random();
            return Enumerable.Range(1, 6).Select(index => new Singer
            {
                Id = rng.Next(1,6),
                Age = rng.Next(16,50),
                Performance = rng.Next(5,40),
                FirstName = Singer_Name[rng.Next(Singer_Name.Length)],
                LastName = Singer_Surname[rng.Next(Singer_Surname.Length)],
                Country = Singer_Country[rng.Next(Singer_Country.Length)] 
            });
        }

        [HttpPost]
        public IActionResult AddSingers([FromBody] Singer singer)
        {
            singerDbContext.Add(singer);


            return Ok();  
        }

        [HttpDelete("{id}")]
        public IActionResult RemoveSingers([FromRoute] int id)
        {
            singerDbContext.Remove(id);


            return Ok(); 
        }

        [HttpPut("{id}")]
        public IActionResult UpdateSingers([FromBody] Singer singer, [FromRoute] int id)
        {
            singerDbContext.Update(id);


            return Ok();
        }
    }
}