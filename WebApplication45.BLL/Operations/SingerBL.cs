﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WebApplication45.Core.Operations;


namespace WebApplication45.BLL.Operations 
{
    public class SingerBL : ISingerBL 
    {
        private readonly SingerDbContext singerDbContext; 
        public Singer AddSingers(Singer singer)
        {
            singerDbContext.Singers.Add(singer);
            singerDbContext.SaveChanges();


            return singer;
        }
         
        public IEnumerable GetSingers()
        {
            throw new NotImplementedException();
        }

        public bool RemoveSingers(int id)
        {
            var rmv = singerDbContext.Singers.Find(id);
            if (rmv == null)
            {
                return false; 
            }

            singerDbContext.Singers.Remove(rmv);
            singerDbContext.SaveChanges();


            return true;
        }

        public bool UpdateSingers(Singer singer, int id)
        {
            var singerToUpdate = singerDbContext.Singers.Find(id);
            if (singerToUpdate == null)
            {
                return false;  
            }


            singerToUpdate.Id = singer.Id;
            singerToUpdate.Age = singer.Age;
            singerToUpdate.Performance = singer.Performance;
            singerToUpdate.FirstName = singer.FirstName;
            singerToUpdate.LastName = singer.LastName;
            singerToUpdate.Country = singer.Country;

            singerDbContext.Singers.Update(singerToUpdate);
            singerDbContext.SaveChanges();


            return true; 
        }
    }
}
